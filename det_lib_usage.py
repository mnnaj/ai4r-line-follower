from detector_lib import *
import os
import cv2

config_path = 'configs/config_1.yaml'
config = load_config(config_path)

ip = ImageProcessor(config)

show = True
test = "dir" # or "stream"

# Select folder
dir_path = "samples/streamcam/new_tape/"

if test == "dir":
    # Iterate through folder
    file_list = os.listdir(dir_path)
    for file in file_list:
        image = cv2.imread(os.path.join(dir_path, file))
        points, img_det = ip.process(image)
        print(points)

        if show:
            # Display the image
            cv2.imshow("Image", img_det)
            cv2.waitKey(0)

    # Close all windows
    cv2.destroyAllWindows()

if test == "stream":
    ## Test with Stream
    res = '480p'
    frame_rate = 30
    exposure = None

    cap = cv2.VideoCapture(0)
    set_capture_properties(cap, res, frame_rate, exposure)

    while True:
        ret, frame = cap.read()
        points, img_det = ip.process(frame)
        print(points)
        if show:
            cv2.imshow("Image", img_det)
            cv2.waitKey(1)