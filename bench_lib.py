import cv2
import numpy as np
from detector_lib import ImageProcessor

def filter_outliers(data, threshold):
    filtered_data = [x for x in data if x <= threshold]
    return filtered_data

def read_yaml_file(file_path):
    with open(file_path, "r") as file:
        return file.read()

def find_nearest_point(test_point, benchmark_points):
    min_distance = float('inf')
    nearest_point = None
    for point in benchmark_points:
        distance = np.linalg.norm(np.array(test_point) - np.array(point))
        if distance < min_distance:
            min_distance = distance
            nearest_point = point
    return nearest_point

def calculate_mse_nearest_point(test_points, benchmark_points):
    total_mse = 0
    for test_point in test_points:
        nearest_point = find_nearest_point(test_point, benchmark_points)
        mse = np.linalg.norm(np.array(test_point) - np.array(nearest_point)) ** 2
        total_mse += mse
    return total_mse / len(test_points)

def distance_to_line(point, line_point1, line_point2):
    # Calculate the perpendicular distance from 'point' to the line formed by 'line_point1' and 'line_point2'
    x0, y0 = point
    x1, y1 = line_point1
    x2, y2 = line_point2

    return np.abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1) / np.sqrt((y2 - y1)**2 + (x2 - x1)**2)

def calculate_mse_for_point(test_point, benchmark_points):
    # Find the two nearest points on the benchmark line to the test point
    nearest_points = sorted(benchmark_points, key=lambda p: np.linalg.norm(np.array(test_point) - np.array(p)))
    nearest_point1, nearest_point2 = nearest_points[:2]
    
    # Calculate MSE for the test point
    mse = distance_to_line(test_point, nearest_point1, nearest_point2)**2
    return mse

def calculate_mse_to_slope(points_test, points_bench):
    total_mse = 0
    for test_point in points_test:
        mse = calculate_mse_for_point(test_point, points_bench)
        total_mse += mse
    return total_mse / len(points_test)

class BenchMSE:
    def __init__(self, config_test, config_bench, draw = False):
        self.ip_test = ImageProcessor(config_test)
        self.ip_bench = ImageProcessor(config_bench)
        self.draw = draw
    
    def benchmark(self, img):
        points_test, _ = self.ip_test.process(img)
        points_bench, _ = self.ip_bench.process(img)
        
        mse_nearest_point = calculate_mse_nearest_point(points_test, points_bench)
        mse_slope = calculate_mse_to_slope(points_test, points_bench)

        # Draw points on the image
        if self.draw:
            for point in points_bench:
                cv2.circle(img, point, 2, (0, 255, 0), -1)
            for point in points_test:
                cv2.circle(img, point, 2, (0, 0, 255), -1)
            return img, mse_nearest_point, mse_slope
        else:
            return None, mse_nearest_point, mse_slope