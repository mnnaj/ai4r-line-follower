# Library Imports

import toml 
import time
import os
import cv2
from collections import deque
import streamlit as st

from detector_lib import *

# Set two column page layout

st.set_page_config(layout='wide')
left_column, right_column = st.columns(2)

config = {}
config_dir = 'configs'

# Parameter selection in the left column:

# Setting up placeholders in the right column

res_list = ["480p", "540p", "720p", "1080p"]

frame_rates = {
    "480p": (5, 10, 15, 20, 30),
    "540p": (5, 10, 15, 20, 30),
    "720p": (5, 10, 15, 20, 30),
    "1080p": (5, 10, 15, 20, 30)
}

# Function to get resolution tuple from selected resolution

# Function moved to detector_lib
# def list_jpg_files_in_directory(directory):
#     jpg_files = [file for file in os.listdir(directory) if file.lower().endswith(".jpg")]
#     return jpg_files

def main():
    # Populate Left Column
    with left_column:
        st.header("Settings")
        st.subheader("Thresholding Parameters")
        st.write("Which channels would you like to select for thresholding?")

        hls_select = st.multiselect(
            "Select HLS Channels",
            ["Hue", "Lightness", "Saturation"],
            default = ["Saturation"]
        )

        config['hls_select'] = hls_select

        st.write("If you select multiple channels, a bitwise AND operation will be performed on the thresholded images.")

        if hls_select:
            for channel in hls_select:
                if channel == "Hue":
                    h_range = st.slider(
                        "Select a threshold range for the Hue channel", 0, 255, (20, 50)
                    )
                    config['h_range'] = h_range
                elif channel == "Lightness":
                    l_range = st.slider(
                        "Select a threshold range for the Lightness channel", 0, 255, (150, 255)
                    )
                    config['l_range'] = l_range
                elif channel == "Saturation":
                    s_range = st.slider(
                        "Select a threshold range for the Saturation channel", 0, 255, (100, 255)
                    )
                    config['s_range'] = s_range
        
        if len(hls_select) == 0:
            st.warning("Please select at least one channel for thresholding")
        
        st.subheader("Image Processing Operations")
        st.write("Select all that apply -")
        morph = st.checkbox("Morphological operations", False)
        if morph:
            config['morph'] = morph
            st.write("Select the morphological operations you would like to perform")

            # Erosion Operation
            erosion = st.checkbox("Erosion", False)
            if erosion:
                kernel_erode = st.slider(
                    "Select the kernel size for erosion",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )
                config['erosion'] = erosion
                config['kernel_erode'] = kernel_erode

            # Dilation Operation
            dilation = st.checkbox("Dilation")
            if dilation:
                kernel_dilate = st.slider(
                    "Select the kernel size for dilation",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )
                config['dilation'] = dilation
                config['kernel_dilate'] = kernel_dilate
            
            # Opening Operation
            opening = st.checkbox("Opening")
            if opening:
                kernel_open = st.slider(
                    "Select the kernel size for opening",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )
                config['opening'] = opening
                config['kernel_open'] = kernel_open

            
            # Closing Operation
            closing = st.checkbox("Closing")
            if closing:
                kernel_close = st.slider(
                    "Select the kernel size for closing",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )
                config['closing'] = closing
                config['kernel_close'] = kernel_close

            st.markdown("""---""")      
        
        largcon = st.checkbox("Get largest contour", False)
        if largcon:
            config['largcon'] = largcon
        
        blur = st.checkbox("Gaussian Blur", False)

        if blur:
            #blur = True
            kernel_blur = st.slider("Select the kernel size for Gaussian Blur",
                                    min_value = 3,
                                    max_value = 21,
                                    step = 2,
                                    value = 3
                                )
            config['blur'] = blur
            config['kernel_blur'] = kernel_blur
        
        st.subheader("Lane Detection")

        # lane_detection = st.radio("Which lane detection method would you like to use?"
        #             , ["Manual", "BN Search"], index = 1)
        # config['lane_detection'] = lane_detection

        st.write("**Search parameters:**")

        # if lane_detection == "Manual":
        #     thinning = st.checkbox("Thinning operation", True)
        #     step_size = st.slider("Select the **step size** for manual search",
        #                             min_value = 10, 
        #                             max_value = 100, 
        #                             step = 10,
        #                             value = 50
        #                         )
        #     config['thinning'] = thinning
        #     config['step_size'] = step_size
                    
        # if lane_detection == "BN Search":
        shape = st.radio("Which shape would you like to use for search?",
                            ("Circle", "Ellipse"), index = 0)
        config['shape'] = shape

        st.write("Please select the search parameters for the search")

        if shape == "Circle":
            radius = st.slider("Select the **radius** for Search",
                                min_value = 10,
                                max_value = 100,
                                step = 10,
                                value = 50
                            )
            start_angle, end_angle = st.slider("Select the **angle range** for Search arc",
                                                min_value = 90,
                                                max_value = 450,
                                                step = 10,
                                                value = (210, 330)
                                            )
            config['radius'] = radius
            config['start_angle'] = start_angle
            config['end_angle'] = end_angle
        elif shape == "Ellipse":
            a = st.slider("Select the **major axis** length",
                            min_value = 20,
                            max_value = 200,
                            step = 10,
                            value = 100)
            b = st.slider("Select the **minor axis** length",
                            min_value = 10,
                            max_value = 100,
                            step = 10,
                            value = 50)
            start_angle, end_angle = st.slider("Select the **angle range** for Search arc",
                                                min_value = 90,
                                                max_value = 450,
                                                step = 10,
                                                value = (210, 330)
                                            )        
            config['a'] = a
            config['b'] = b
            config['start_angle'] = start_angle
            config['end_angle'] = end_angle

    # Populate Right Column
    with right_column:
        st.subheader("Source")
        src = st.radio("Select image source: ", ("Path", "Directory", "Stream"), index = 0, horizontal = True)
        if src == "Path":
            dir_path = st.text_input("Enter path to directory", "samples/")
            if dir_path:
                if os.path.exists(dir_path):
                    # Get the list of files in the selected directory
                    files = list_jpg_files_in_directory(dir_path)
                    # Display a drop-down box with all files in the directory
                    image_path = st.selectbox("Select a file:", files)
                    image_path = os.path.join(dir_path, image_path)
        elif src == "Directory":
            dir_root = st.text_input("Enter path to directory root", "samples/streamcam/")
            if dir_root:
                if os.path.exists(dir_root):
                    # Get the list of dirs in the selected directory
                    dirs = os.listdir(dir_root)
                    # Display a drop-down box with all dirs in the directory
                    dir_path = st.selectbox("Select a directory:", dirs)
                    dir_path = os.path.join(dir_root, dir_path)
        elif src == "Stream":
            res = st.radio("Select resolution: ", res_list, index = 0, horizontal = True)
            fr_opts = frame_rates[res]
            frame_rate = st.radio("Select frame rate: ", fr_opts, index = len(fr_opts) - 1, horizontal = True)
            exposure = st.slider("Exposure: ", min_value=-4, max_value=4, value=0, step=1)
            #st.warning("These settings are not yet implemented")
            #stream_fps_placeholder = st.empty()
            #stream_res_placeholder = st.empty()
            stream_info_placeholder = st.empty()

        st.header("Visualization")
        threshold_view = st.checkbox("View Thresholded Image", False)
        other_view = st.checkbox("View Output of Image Processing Ops", False)
        draw = st.checkbox("View Lane Detection Output", True)
        config['draw'] = draw   
        
        if threshold_view: 
            st.subheader("Thresholding")
            threshold_placeholder = st.empty()
        if other_view:
            st.subheader("Output of Image Processing Operations")
            other_placeholder = st.empty()
        st.subheader("Lane Detection")
        stop = st.button("Stop")
        if draw:
            lane_placeholder = st.empty()
        else:
            fps_placeholder = st.empty()
            ft_placeholder = st.empty()
        
        st.write("Stop the stream first and click the button below to save the current configuration")
        config_name = st.text_input("Enter a name for the configuration", "config_1")
        save = st.button("Save Config")

    # Setup Image Processor
    ip = ImageProcessor(config)

    def process_image(image):
        ip.load_image(image)
        im_thresh = ip.threshold_image()
        if threshold_view:
            threshold_placeholder.image(
                im_thresh, caption="Thresholded Image", use_column_width=True)
        im_proc = ip.preprocess_image()
        if other_view:
            other_placeholder.image(
                im_proc, caption="Processed Image", use_column_width=True)
        points, img_det = ip.detect_line()
        return img_det

    buffer_size = 50

    if src == "Path":
        try:
            image = cv2.imread(image_path)
            img_det = process_image(image)
            if draw:
                lane_placeholder.image(
                    img_det, caption = "Lane Detection", use_column_width = True)

        except Exception as e:
            #st.error(f"Error loading the selected image: {e}")
            lane_placeholder.warning("Please select a valid image file")
    elif src == "Directory":
        try:
            file_list = os.listdir(dir_path)
            timestamps = deque(maxlen = buffer_size)
            frametimes = deque(maxlen = buffer_size)
            while not stop:
                for file in file_list:
                    timestamps.append(time.time())
                    image = cv2.imread(os.path.join(dir_path, file))
                    start_time = time.time()
                    img_det = process_image(image)
                    end_time = time.time()
                    frametimes.append(end_time - start_time)

                    if len(timestamps) == buffer_size:
                        fps_real = buffer_size / (timestamps[-1]-timestamps[0])
                        fps_algo = buffer_size / sum(frametimes)
                        frametimes_avg = 1000 / fps_algo
                        if draw:
                            cv2.putText(img_det, f"FPS (real): {fps_real:.2f}, (algo): {fps_algo:.2f}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                            cv2.putText(img_det, f"Frame Time: {frametimes_avg:.2f} ms", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
                        else:
                            fps_placeholder.write(f"FPS (real): {fps_real:.2f}, (algo): {fps_algo:.2f}")
                            ft_placeholder.write(f"Frame Time: {frametimes_avg:.2f} ms")
                    if draw:
                        lane_placeholder.image(
                            img_det, caption = "Lane Detection", use_column_width = True)
        except:
            lane_placeholder.warning("Please select a valid directory")
    elif src == "Stream":
        cap = cv2.VideoCapture(0)
        # Set capture properties
        set_capture_properties(cap, res, frame_rate, exposure)
        # Display stream properties in the placeholders
        current_frame_rate = cap.get(cv2.CAP_PROP_FPS)
        #stream_fps_placeholder.text(f"Current frame rate: {current_frame_rate:.2f}")
        current_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        current_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        current_exposure = cap.get(cv2.CAP_PROP_EXPOSURE)
        #stream_res_placeholder.text(f"Current resolution: {current_width} x {current_height}")
        STREAM_INFO = """
        ```
        Stream Information
        ------------------
        Resolution: {current_width} x {current_height}
        Frame Rate: {current_frame_rate:.2f}
        Exposure: {current_exposure:.2f}
        ```
        """.format(
            current_width = current_width,
            current_height = current_height,
            current_frame_rate = current_frame_rate,
            current_exposure = current_exposure
        )

        stream_info_placeholder.markdown(STREAM_INFO)

        timestamps = deque(maxlen = buffer_size)
        frametimes = deque(maxlen = buffer_size)
        while not stop:
            timestamps.append(time.time())
            ret, frame = cap.read()
            if frame is not None:
                start_time = time.time()
                img_det = process_image(frame)
                end_time = time.time()
                frametimes.append(end_time - start_time)
                if len(timestamps) == buffer_size:
                    fps_real = buffer_size / (timestamps[-1]-timestamps[0])
                    fps_algo = buffer_size / sum(frametimes)
                    frametimes_avg = 1000 / fps_algo
                    if draw:
                        cv2.putText(img_det, f"FPS (real): {fps_real:.2f}, (algo): {fps_algo:.2f}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                        cv2.putText(img_det, f"Frame Time: {frametimes_avg:.2f} ms", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
                    else:
                        fps_placeholder.write(f"FPS (real): {fps_real:.2f}, (algo): {fps_algo:.2f}")
                        ft_placeholder.write(f"Frame Time: {frametimes_avg:.2f} ms")
                if draw:
                    lane_placeholder.image(
                        img_det, caption = "Lane Detection", use_column_width = True)
            else:
                lane_placeholder.text("Waiting for the camera to initialize...")
                time.sleep(1)
                cap.release()
                cap = cv2.VideoCapture(0)
                set_capture_properties(cap, res, frame_rate, exposure)
                # Display stream properties in the placeholders
            #current_frame_rate = cap.get(cv2.CAP_PROP_FPS)
            #stream_fps_placeholder.text(f"Current frame rate: {current_frame_rate:.2f}")
            #current_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
            #current_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
            #stream_res_placeholder.text(f"Current resolution: {current_width} x {current_height}")
            #stream_info_placeholder.markdown(STREAM_INFO)

        cap.release()

    if save:
        config_path = os.path.join(config_dir, config_name + ".yaml")
        # make sure the path exists 
        if not os.path.exists(config_dir):
            os.makedirs(config_dir)
        save_config(config, config_path)
        st.write("Config saved successfully!")

if __name__ == '__main__':
    main()