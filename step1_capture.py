import cv2
import os
import re
import streamlit as st
import time

st.set_page_config(layout='wide')

RES_LIST = ["480p", "540p", "720p", "1080p"]

FRAME_RATES = {
    "480p": (5, 10, 15, 20, 30),
    "540p": (5, 10, 15, 20, 30),
    "720p": (5, 10, 15, 20, 30),
    "1080p": (5, 10, 15, 20, 30)
}

OUTPUT_DIR = 'capture'
os.makedirs(OUTPUT_DIR, exist_ok=True)

INSTRUCTIONS = """
    ## Instructions

    1. Specify the number of frames to capture in the "Enter number of frames to capture" input box. 
    2. Choose your desired resolution from the "Select resolution" radio button options.
    3. Choose your desired frame rate based on the selected resolution from the "Select frame rate" radio button options.
    4. Use the "Exposure" slider to adjust the camera's exposure setting.
    5. Click "Start Capture" to begin capturing frames. The application will automatically capture the next N frames and store them.
    6. Click "Stop Stream" to stop the stream and end the frame capturing process. **Wait for the application to save the captured frames to disk before you stop the stream.**
    
    Note: The application will automatically save the captured frames in a directory named after your run in the 'capture' folder.
    """

def get_resolution(resolution):
    """Get the resolution tuple from selected resolution"""
    resolutions = {
        "480p": (640, 480),
        "540p": (960, 540),
        "720p": (1280, 720),
        "1080p": (1920, 1080)
    }
    return resolutions[resolution]


def set_capture_properties(capture, resolution, frame_rate, exposure):
    """Set video capture properties"""
    width, height = get_resolution(resolution)
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    capture.set(cv2.CAP_PROP_FPS, frame_rate)
    capture.set(cv2.CAP_PROP_EXPOSURE, exposure)

def get_next_run(directory_path):
    """Get the next run number"""
    subdirs = [d for d in os.listdir(directory_path) if os.path.isdir(os.path.join(directory_path, d)) and d.startswith('run_')]
    run_numbers = [int(re.search(r'\d+', subdir).group()) for subdir in subdirs]
    max_run_number = max(run_numbers) if subdirs else 0
    return f'run_{max_run_number + 1}'


def save_frames(frame_buffer, output_dir):
    """Save frames to disk"""
    for i, frame in enumerate(frame_buffer):
        cv2.imwrite(f"{output_dir}/frame_{i}.jpg", frame)


def initialize_session_state():
    """Initialize the session state"""
    if 'frame_buffer' not in st.session_state:
        st.session_state.frame_buffer = []
        st.session_state.counter = 0


def display_current_frame(frame_placeholder, frame):
    """Display the current frame"""
    if frame is not None:
        frame_placeholder.image(frame, channels="BGR")
    else:
        frame_placeholder.text("Waiting for the camera to initialize ...")


def main():
    st.title("Frame Capture Tool")

    # Create two columns
    col1, col2 = st.columns(2)

    show_instructions = col1.checkbox("Show Instructions", value = True)
    instructions_placeholder = col1.empty()

    if show_instructions:
        instructions_placeholder.markdown(INSTRUCTIONS)

    next_run = get_next_run(OUTPUT_DIR)
    name_run = col1.text_input("Name of run: ", value=next_run, disabled = True)
    out_run = os.path.join(OUTPUT_DIR, name_run)

    frame_limit = col1.number_input(
        "Enter number of frames to capture: ",
        min_value=1, max_value=1000, value=100, step=1)

    res = col1.radio("Select resolution: ", RES_LIST, index=0, key="resolution", horizontal = True)
    frame_rate = col1.radio("Select frame rate: ", FRAME_RATES[res], index=len(FRAME_RATES[res]) - 1, key="frame_rate", horizontal = True)
    exposure = col1.slider("Exposure: ", min_value=-4, max_value=4, value=0, step=1)

    stream_info_placeholder = col2.empty()
    frame_placeholder = col2.empty()
    info_placeholder = col2.empty()

    cap = cv2.VideoCapture(0)
    set_capture_properties(cap, res, frame_rate, exposure)

    start_button = col2.button('Start Capture', key='start')
    stop_button = col2.button('Stop Stream', key='stop')

    initialize_session_state()

    current_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    current_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    current_frame_rate = cap.get(cv2.CAP_PROP_FPS)
    current_exposure = cap.get(cv2.CAP_PROP_EXPOSURE)

    STREAM_INFO = """
    ```
    Stream Information
    ------------------
    Resolution: {current_width} x {current_height}
    Frame Rate: {current_frame_rate:.2f}
    Exposure: {exposure:.2f}
    ```
    """.format(
        current_width = current_width,
        current_height = current_height,
        current_frame_rate = current_frame_rate,
        exposure = current_exposure
    )

    stream_info_placeholder.markdown(STREAM_INFO)

    while not stop_button:
        ret, frame = cap.read()
        display_current_frame(frame_placeholder, frame)

        if frame is not None:
            if start_button and st.session_state.counter < frame_limit:
                os.makedirs(out_run, exist_ok=True)
                st.session_state.frame_buffer.append(frame)
                st.session_state.counter += 1
                info_placeholder.text(f"Capturing frame {st.session_state.counter} of {frame_limit} ...")
            elif len(st.session_state.frame_buffer) == frame_limit:
                info_placeholder.text("Saving frames ...")
                save_frames(st.session_state.frame_buffer, out_run)
                info_placeholder.text(f"Done! Results stored to 'capture/{name_run}'")
                st.session_state.frame_buffer.clear()
                st.session_state.counter = 0
                start_button = False
        else:
            time.sleep(1)
            cap.release()
            cap = cv2.VideoCapture(0)
            set_capture_properties(cap, res, frame_rate, exposure)

    cap.release()


if __name__ == '__main__':
    main()