import toml
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
from process import ImageProcessor, BNSearch

cfg_path = 'data/cfg_thin.toml'
#cfg_path = 'data/cfg_bns.toml'
#cfg_path = 'data/cfg_nodist.toml'

# List of image paths
image_names = [
    "gray_carpet_straight",
    "gray_carpet_left",
    "gray_carpet_right",
    "gray_carpet_sharp_left",
    "gray_carpet_sharp_right",
    "gray_lab_straight",
    "hallway",
    "red_surface",
]

# Load config file

cfg = toml.load(cfg_path)

# Setup Image Processor:

processor = ImageProcessor(cfg)

# Load image
selected_image = image_names[0]

timestamps = {}

timestamps['start'] = time.time()

image = cv2.imread('samples/{}.jpg'.format(selected_image))
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

timestamps['load'] = time.time()

# UnDistort image
# if cfg['undistort']:
#     undistorted = processor.undistort(image)
#     timestamps['undistort'] = time.time()
# else:
processor.image = image

# Apply Color Balance
if cfg['color_balance']:
    balanced = processor.color_balance()
    timestamps['color_balance'] = time.time()
    
# Apply Warp
if cfg['warp']:
    points = np.float32(cfg['warp'])
    warped = processor.warp(points)
    timestamps['warp'] = time.time()

h, l, s = processor.get_hls()

threshold_channels = cfg['threshold_channels']

thresholded_images = []

for channel in threshold_channels:
    if channel == 'Hue':
        h_thresh = tuple(cfg['h_thresh'])
        h_thresh = processor.threshold(h, thresh = h_thresh)
        thresholded_images.append(h_thresh)
    elif channel == 'Lightness':
        l_thresh = tuple(cfg['l_thresh'])
        l_thresh = processor.threshold(l, thresh = l_thresh)
        thresholded_images.append(l_thresh)
    elif channel == 'Saturation':
        s_thresh = tuple(cfg['s_thresh'])
        s_thresh = processor.threshold(s, thresh = s_thresh)
        thresholded_images.append(s_thresh)

# Combine the thresholded images

binary = processor.combine(thresholded_images)
timestamps['threshold'] = time.time()

# Apply large contour filter
if(cfg['largcon']):
    contours, binary = processor.get_largest_contour(binary)
    timestamps['largcon'] = time.time()

# Apply blur
if(cfg['blur']):
    kernel_size = cfg['blur_kernel']
    binary = processor.blur(binary, kernel_size)
    timestamps['blur'] = time.time()

lane_detection = cfg['lane_detection']

# Apply lane detection using thinning
if lane_detection == 'Thinning':
    #thinned = processor.thinning(binary)
    step_size = cfg['thin_step']
    timestamps['thinning'] = time.time()

    points = processor.get_points(binary, step_size)
    timestamps['get_points'] = time.time()

    #df = pd.DataFrame(points, columns = ['x', 'y'])
    #print(df)
    # print('Number of points found: ', len(points))
    # print(points)

    # thinned = processor.plot_points(thinned, points)
    # plt.imshow(thinned, cmap = 'gray')
    # plt.show()

# Apply lane detection using BNSearch

draw = False

if lane_detection == 'BN Search':
    shape = cfg['shape']

    start_angle, end_angle = cfg['angles']

    if shape == 'Circle':
        radius = cfg['radius']

        searcher = BNSearch(
            search_method = 'circle',
            radius = radius,
            start_angle = start_angle,
            end_angle = end_angle,
            draw = draw
        )
    
    if shape == 'Ellipse':
        a, b = cfg['ellipse_ab']

        searcher = BNSearch(
            search_method = 'ellipse',
            a = a,
            b = b,
            start_angle = start_angle,
            end_angle = end_angle,
            draw = draw
        )

    points = searcher(binary)

    timestamps['BNS'] = time.time()

    bns_time = time.time()

    points_image = searcher.img_color

    #plt.imshow(points_image)
    #plt.show()

    #print('Number of points found: ', len(points))
    #print(points)

print('Summary of times with {} method:\n'.format(lane_detection))
print('{:<10s} {:>10s}'.format('Process', 'Time (ms)'))
print('-' * 25)

last = timestamps['start']

for key in timestamps:
    if key == 'start':
        continue
    time_ms = 1000*(timestamps[key] - last)
    print('{:<10s} {:>10.3f}'.format(key, time_ms))
    last = timestamps[key]

print('-' * 25)

total_time_ms = 1000*(last - timestamps['start'])
print('{:<10s} {:>10.3f}'.format('Total', total_time_ms))
