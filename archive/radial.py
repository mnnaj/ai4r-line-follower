# Import libraries

import time
import cv2
import numpy as np
from numba import jit

import matplotlib.pyplot as plt

radius = 50

# def create_circle_hash_table(radius):
#     """
#     Create a hash table for circle points
#     """
#     hash_table = {}
#     for angle in range(361):  # Including 360 degrees
#         x = int(radius * np.cos(np.deg2rad(angle)))
#         y = int(radius * np.sin(np.deg2rad(angle)))
#         hash_table[angle] = (x, y)
#     return hash_table

# def create_circle_hash_table(radius):
#     """
#     Create a hash table for circle points
#     """
#     hash_table = {}
#     angles = np.arange(361)
#     hash_table['x'] = int(radius * np.cos(np.deg2rad(angles)))
#     hash_table['y'] = int(radius * np.sin(np.deg2rad(angles)))
#     return hash_table

def create_circle_hash_table(radius):
    hash_table = {
        'x': np.zeros(361, dtype=np.int8),
        'y': np.zeros(361, dtype=np.int8)
    }
    for angle in range(361):
        hash_table['x'][angle] = int(radius * np.cos(np.deg2rad(angle)))
        hash_table['y'][angle] = int(radius * np.sin(np.deg2rad(angle)))
    return hash_table

hash_table = create_circle_hash_table(radius)

timestamps = {}

timestamps['start'] = time.time()

# Load image

image = cv2.imread('samples/gray_carpet_straight.jpg')
#image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
#image_path = 'samples/streamcam/new_tape/test_2023-05-31_11-58-29.jpg'
#image = cv2.imread(image_path)

#plt.imshow(image)

timestamps['load'] = time.time()

# We are skipping the following steps: undistort, color balance, warp

# HLS Thresholding: 

#hls = cv2.cvtColor(image, cv2.COLOR_RGB2HLS)

# We can optimize this step by directly converting the original image
# from BGR to HLS instead of taking two separate steps to do it.

hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
# h_channel = hls[:, :, 0]
# l_channel = hls[:, :, 1]
# s_channel = hls[:, :, 2]

# Can we simplify the above step?
_, _, s_channel = cv2.split(hls)

#plt.imshow(s_channel, cmap='gray')

timestamps['hls'] = time.time()

#h_thresh = (20, 50)
#s_thresh = (100, 255)

# def threshold(img, thresh):
#     binary = np.zeros_like(img)
#     binary[(img > thresh[0]) & (img <= thresh[1])] = 255
#     return binary

# @jit(nopython=True)
# # This is just slow
# def threshold(img, thresh):
#     binary = np.zeros_like(img)
#     for i in range(img.shape[0]):
#         for j in range(img.shape[1]):
#             if img[i, j] > thresh[0] and img[i, j] <= thresh[1]:
#                 binary[i, j] = 255
#     return binary


#h_binary = threshold(h_channel, h_thresh)
#s_binary = threshold(s_channel, s_thresh)

#h_binary = cv2.threshold(h_channel, 20, 50, cv2.THRESH_BINARY)[1]
img = cv2.threshold(s_channel, 100, 255, cv2.THRESH_BINARY)[1]

# Despite being more concise, this implementation is slower
# def threshold(img, thresh):
#     return np.where((img > thresh[0]) & (img <= thresh[1]), 255, 0)

# This implementation doesn't provide significant enough performance boost to justify
# def threshold(img, thresh, binary):
#     binary[(img > thresh[0]) & (img <= thresh[1])] = 255
#     return binary

# binary_img_shape = s_channel.shape
# h_binary = np.zeros(binary_img_shape, dtype=np.uint8)
# s_binary = np.zeros(binary_img_shape, dtype=np.uint8)

# h_binary = threshold(h_channel, h_thresh, h_binary)
# s_binary = threshold(s_channel, s_thresh, s_binary)

# def threshold(img, thresh, binary):
#     binary[(img > thresh[0]) & (img <= thresh[1])] = 255
#     return binary

#img = cv2.bitwise_and(h_binary, s_binary)

timestamps['threshold'] = time.time()

def get_starting_point():
    """
    Function to get the starting point of the lane on an image
    The starting point is ideally at the bottom of the image
    If the lane is not found at bottom center
    The function will move up the image until it finds the lane
    If the lane is not found in the bottom half,
    the function will return None
    """
    img_shape_0 = img.shape[0]
    last_idx = -1
    while abs(last_idx) < img_shape_0//2:
        last_row = img[last_idx, :]
        lane_exists = np.where(last_row > 0)[0] # lane exists where there is a nonzero value
        if len(lane_exists) > 0: # If lane exists
            lane_center = np.median(lane_exists)
            center_point = (int(lane_center), (img_shape_0 + last_idx))
            return center_point
        # Else move up by 1 pixel
        last_idx -+ 1
    return None

# def get_starting_point():
#     """
#     Function to get the starting point of the lane on an image
#     The starting point is ideally at the bottom of the image
#     If the lane is not found at bottom center
#     the function will move up the image until it finds the lane
#     If the lane is not found in the bottom half,
#     the function will return None
#     """
#     img_shape_0 = img.shape[0]
#     last_idx = -1
#     last_row = img[-1, :]  # Start with the last row of the image

#     while abs(last_idx) < img_shape_0 // 2:
#         lane_exists = np.any(last_row > 0)  # Check if any nonzero value exists

#         if lane_exists:
#             lane_center = np.median(np.where(last_row > 0)[0])
#             center_point = (int(lane_center), img_shape_0 - 1)
#             return center_point

#         last_row = img[last_idx - 1, :]  # Move up by 1 row

#     return None

start = get_starting_point()

timestamps['start_point'] = time.time()

#radius = 50
start_angle = 210
end_angle = 330

# def get_points_on_circle(center):
#     """
#     Get points on a circle
#     """
#     points = []
#     for angle in np.arange(start_angle, end_angle, 1):
#         x = int(center[0] + radius * np.cos(np.deg2rad(angle)))
#         y = int(center[1] + radius * np.sin(np.deg2rad(angle)))
#         points.append((int(x), int(y)))
#     return points

# The hash table speed things up by around 2.8x
# def get_points_on_circle(center):#, radius, start_angle=0, end_angle=360):
#     """
#     Get points on a circle using the hash table
#     """
#     points = []
#     for angle in range(start_angle, end_angle):
#         x_offset, y_offset = hash_table[angle]
#         x = int(center[0] + x_offset)
#         y = int(center[1] + y_offset)
#         points.append((x, y))
#     return points

start = get_starting_point()

start_angle = 210
end_angle = 330

def get_points_on_circle(center, delta = 0):#, radius, start_angle=0, end_angle=360):
    """
    Get points on a circle using the hash table
    """
    angles = np.arange(start_angle + delta, end_angle + delta)
    x_offsets = hash_table['x'][angles]
    y_offsets = hash_table['y'][angles]
    points = np.column_stack((x_offsets + center[0], y_offsets + center[1]))
    return points.tolist()

def get_next_point(current_point, delta = 0):
    points = get_points_on_circle(current_point, delta)

    # TODO: Get values on circle maybe?  def get_values_on_circle()

    # TODO: This might be sped up
    # Check if any point is out of bounds
    for point in points:
        if point[0] < 0 or point[0] > img.shape[1] or point[1] < 0 or point[1] > img.shape[0]:
            return None
  
    #breakpoint()

    # TODO: This might be sped up: May try to implement a np.median or sth like that?
    points_on_lane = []
    for point in points:
        if img[point[1], point[0]] > 0:
            points_on_lane.append(point)
    
    if len(points_on_lane) == 0:
        return None
    
    median_idx = int(len(points_on_lane)/2)
    next_point = points_on_lane[median_idx]

    return next_point

points = [start, ]
next_point = get_next_point(start)
points.append(next_point)

while next_point is not None:
    # TODO: This might be sped up
    # Calculate the angle between the last two points
    theta = np.arctan2(points[-1][1] - points[-2][1], points[-1][0] - points[-2][0])
    delta = int(90 + np.rad2deg(theta))
    # Get next point and append to points list
    next_point = get_next_point(next_point, delta = delta)
    if next_point is not None:
        points.append(next_point)

timestamps['next_point'] = time.time()

print('Summary of times with Radial Search method:\n')
print('{:<10s} {:>10s}'.format('Process', 'Time (ms)'))
print('-' * 25)

last = timestamps['start']

for key in timestamps:
    if key == 'start':
        continue
    time_ms = 1000*(timestamps[key] - last)
    print('{:<10s} {:>10.3f}'.format(key, time_ms))
    last = timestamps[key]

print('-' * 25)

total_time_ms = 1000*(last - timestamps['start'])
print('{:<10s} {:>10.3f}'.format('Total', total_time_ms))

print('=' * 25)

# print the point co-ordinates

# for point in points:
#     print(point)

#plt.imshow(img, cmap='gray')
#plt.show()