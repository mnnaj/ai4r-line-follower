import cv2
import numpy as np
import matplotlib.pyplot as plt

# List of image paths
image_names = [
    "gray_carpet_straight",
    "gray_carpet_left",
    "gray_carpet_right",
    "gray_carpet_sharp_left",
    "gray_carpet_sharp_right",
    "gray_lab_straight",
    "hallway",
    "red_surface",
]

selected_image = image_names[0]

image = cv2.imread('samples/{}.jpg'.format(selected_image))
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

p1 = (0.28, 0.45)
p2 = (0.72, 0.45)
p3 = (0.0, 0.9)
p4 = (1.0, 0.9)

src_points = np.array([p1, p2, p3, p4], dtype=np.float32)
dst_points = np.array([(0, 0), (1, 0), (0, 1), (1, 1)], dtype=np.float32)

def draw_points(image, src):
    image_points = image.copy()
    img_size = np.float32([(image.shape[1],image.shape[0])])
    src = src * img_size

    pts = np.int32(src).reshape((-1,1,2))
    pts = pts[[0,1,3,2],:,:]

    image_points = cv2.polylines(image_points,[pts],True,(0,255,255),2)
    return image_points

def warp(image,
        src,
        dst=np.float32([(0,0), (1, 0), (0,1), (1,1)]),
        dst_size=(1280, 720)):
    img_size = np.float32([(image.shape[1],image.shape[0])])
    src = src * img_size
    dst = dst * np.float32(dst_size)
    # Given src and dst points, calculate the perspective transform matrix
    M = cv2.getPerspectiveTransform(src, dst)
    # Warp the image using OpenCV warpPerspective()
    warped = cv2.warpPerspective(image, M, dst_size)
    return warped

def warp(image,
        src,
        dst=np.float32([(0,0), (1, 0), (0,1), (1,1)]),
        dst_size=(1280, 720),
        pad=0):
    img_size = np.float32([(image.shape[1],image.shape[0])])
    src = src * img_size

    # Calculate the required padding based on the destination points
    max_x, max_y = np.max(dst, axis=0)
    min_x, min_y = np.min(dst, axis=0)
    pad_x = max(0, int(np.ceil(pad * image.shape[1])))
    pad_y = max(0, int(np.ceil(pad * image.shape[0])))
    dst += np.float32([(pad_x, pad_y)])
    dst_size = (int(np.ceil((max_x - min_x) * image.shape[1] + pad_x)),
                int(np.ceil((max_y - min_y) * image.shape[0] + pad_y)))

    dst = dst * np.float32(dst_size)

    # Given src and dst points, calculate the perspective transform matrix
    M = cv2.getPerspectiveTransform(src, dst)

    # Warp the image using OpenCV warpPerspective() with padding
    padded_image = cv2.copyMakeBorder(image, pad_y, pad_y, pad_x, pad_x, cv2.BORDER_CONSTANT, value=(0, 0, 0))
    warped = cv2.warpPerspective(padded_image, M, dst_size)

    return warped


points_image = draw_points(image, src_points)
warped_image = warp(image, src_points, dst_points)

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))
ax1.imshow(points_image)
ax1.set_title('Points')
ax2.imshow(warped_image)
ax2.set_title('Warped Image')
plt.show()