import cv2
import time
import numpy as np
from collections import deque

cap = cv2.VideoCapture(0)

detect_line = False

# --------------------- SOURCE CODE FOR LINE DETECTION ---------------------

# Params
radius = 50
start_angle = 210
end_angle = 330
#img_shape_0 = 1080

def create_circle_hash_table(radius):
    hash_table = {
        'x': np.zeros(361, dtype=np.int8),
        'y': np.zeros(361, dtype=np.int8)
    }
    for angle in range(361):
        hash_table['x'][angle] = int(radius * np.cos(np.deg2rad(angle)))
        hash_table['y'][angle] = int(radius * np.sin(np.deg2rad(angle)))
    return hash_table

hash_table = create_circle_hash_table(radius)

def get_starting_point(img):
    img_shape_0 = img.shape[0]
    last_idx = -1
    while abs(last_idx) < img_shape_0//2:
        last_row = img[last_idx, :]
        lane_exists = np.where(last_row > 0)[0] # lane exists where there is a nonzero value
        if len(lane_exists) > 0: # If lane exists
            lane_center = np.median(lane_exists)
            center_point = (int(lane_center), (img_shape_0 + last_idx))
            return center_point
        # Else move up by 1 pixel
        last_idx -+ 1
    return None

# This code has been optimized to use the hash table
def get_points_on_circle(center, delta = 0):#, radius, start_angle=0, end_angle=360):
    """
    Get points on a circle using the hash table
    """
    angles = np.arange(start_angle + delta, end_angle + delta)
    x_offsets = hash_table['x'][angles]
    y_offsets = hash_table['y'][angles]
    points = np.column_stack((x_offsets + center[0], y_offsets + center[1]))
    return points

# This code has been optimized for speed & efficiency
def get_next_point(img, current_point, delta = 0):
    points = get_points_on_circle(current_point, delta)

    # Check if any point is out of bounds
    valid_points_mask = np.logical_and.reduce((
        points[:, 0] >= 0,
        points[:, 0] < img.shape[1],
        points[:, 1] >= 0,
        points[:, 1] < img.shape[0]
    ))

    # Return None if any of the points is invalid
    if not np.all(valid_points_mask): return None
    
    # TODO: Instead of stopping, we can just filter out valid points, 
    # and only stop if there are no more valid points to search for
    
    valid_mask = img[tuple(points.T)] > 0
    points_on_lane = points[valid_mask]

    # Return none if no point is found
    if len(points_on_lane) == 0: return None
    
    # Return the median point (next point)
    return points_on_lane[int(len(points_on_lane)/2)]

# ---------------------------------------------------------------------------

# Check if the video stream is opened successfully
if not cap.isOpened():
    print("Error opening video stream")
    exit()

buffer_size = 50
timestamps = deque(maxlen=buffer_size)

while True:
    ret, frame = cap.read() 

    if not ret:
        break

    # add timestamp here
    
    # wait for 5 ms
    time.sleep(0.005)

    if detect_line:
        hls = cv2.cvtColor(frame, cv2.COLOR_BGR2HLS)
        _, _, s_channel = cv2.split(hls)

        img = cv2.threshold(s_channel, 50, 255, cv2.THRESH_BINARY)[1]
        start = get_starting_point(img)

        if start is not None:
            points = [start, ]
            next_point = get_next_point(img, start)

            while next_point is not None:
                theta = np.arctan2(points[-1][1] - points[-2][1], points[-1][0] - points[-2][0])
                delta = int(90 + np.rad2deg(theta))

            # Get next point and append to points list
            next_point = get_next_point(img, next_point, delta)
            if next_point is not None: points.append(next_point)

            # Draw points on frame
            for point in points:
                cv2.circle(frame, point, 3, (0, 255, 0), -1)

    # Add current timestamp to the buffer
    timestamps.append(time.time())

    # Calculate the frame rate for the last 100 frames
    if len(timestamps) >= buffer_size:
        start_time = timestamps[0]
        end_time = timestamps[-1]
        elapsed_time = end_time - start_time
        fps = buffer_size / elapsed_time

        # Overlay the frame rate on the image
        cv2.putText(frame, "FPS: {:.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow('frame', frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
