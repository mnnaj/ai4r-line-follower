import time
import cv2
import numpy as np

import matplotlib.pyplot as plt

# Params
radius = 50
start_angle = 210
end_angle = 330

def create_circle_hash_table(radius):
    hash_table = {
        'x': np.zeros(361, dtype=np.int8),
        'y': np.zeros(361, dtype=np.int8)
    }
    for angle in range(361):
        hash_table['x'][angle] = int(radius * np.cos(np.deg2rad(angle)))
        hash_table['y'][angle] = int(radius * np.sin(np.deg2rad(angle)))
    return hash_table

hash_table = create_circle_hash_table(radius)

timestamps = {}

#timestamps['start'] = time.time()

# Load image

image_path = 'samples/gray_carpet_straight.jpg'
image = cv2.imread(image_path)

#timestamps['load'] = time.time()
timestamps['start'] = time.time()


hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
_, _, s_channel = cv2.split(hls)

timestamps['hls'] = time.time()

img = cv2.threshold(s_channel, 100, 255, cv2.THRESH_BINARY)[1]
#breakpoint()


timestamps['threshold'] = time.time()

# This code has been optimized
def get_starting_point():
    """
    Function to get the starting point of the lane on an image
    The starting point is ideally at the bottom of the image
    If the lane is not found at bottom center
    The function will move up the image until it finds the lane
    If the lane is not found in the bottom half,
    the function will return None
    """
    img_shape_0 = img.shape[0]
    last_idx = -1
    while abs(last_idx) < img_shape_0//2:
        last_row = img[last_idx, :]
        lane_exists = np.where(last_row > 0)[0] # lane exists where there is a nonzero value
        if len(lane_exists) > 0: # If lane exists
            lane_center = np.median(lane_exists)
            center_point = (int(lane_center), (img_shape_0 + last_idx))
            return center_point
        # Else move up by 1 pixel
        last_idx -+ 1
    return None

start = get_starting_point()
timestamps['start_point'] = time.time()

# This code has been optimized to use the hash table
def get_points_on_circle(center, delta = 0):#, radius, start_angle=0, end_angle=360):
    """
    Get points on a circle using the hash table
    """
    angles = np.arange(start_angle + delta, end_angle + delta)
    x_offsets = hash_table['x'][angles]
    y_offsets = hash_table['y'][angles]
    points = np.column_stack((x_offsets + center[0], y_offsets + center[1]))
    return points

# This doesn't exactly minimize the memory access times, it just delays the inevitable
# def get_points_and_values_on_circle(center, delta=0):
#     """
#     Get points on a circle along with corresponding image values
#     """
#     angles = np.arange(start_angle + delta, end_angle + delta)
#     x_offsets = hash_table['x'][angles]
#     y_offsets = hash_table['y'][angles]
#     points = np.column_stack((x_offsets + center[0], y_offsets + center[1]))
    
#     # Get image values at the points on the circle
#     #values = img[points[:, 1], points[:, 0]]
#     values = img[tuple(points.T)]
    
#     return points, values


# This code needs optimization
def get_next_point(current_point, delta = 0):
    points = get_points_on_circle(current_point, delta)
    #points, values = get_points_and_values_on_circle(current_point, delta)

    # TODO: We would only need to get the values on the circle to make the final judgement. 
    # Would it be possible to also return values on the circle at the same time 
    # With the function get_points_on_circle? Would it speed things up? 

    # Check if any point is out of bounds
    valid_points_mask = np.logical_and.reduce((
        points[:, 0] >= 0,
        points[:, 0] < img.shape[1],
        points[:, 1] >= 0,
        points[:, 1] < img.shape[0]
    ))

    if not np.all(valid_points_mask):
        return None
    
    # This has been modified and optimized for memory access
    valid_mask = img[tuple(points.T)] > 0
    #valid_mask = values > 0
    points_on_lane = points[valid_mask]
    
    # points_on_lane = points[values > 0]

    if len(points_on_lane) == 0:
        return None
    
    median_idx = int(len(points_on_lane)/2)
    next_point = points_on_lane[median_idx]

    return next_point

points = [start, ]
next_point = get_next_point(start)
points.append(next_point)

while next_point is not None:
    # Speedup not possible
    # Calculate the angle between the last two points
    theta = np.arctan2(points[-1][1] - points[-2][1], points[-1][0] - points[-2][0])
    delta = int(90 + np.rad2deg(theta))
    # Get next point and append to points list
    next_point = get_next_point(next_point, delta = delta)
    if next_point is not None:
        points.append(next_point)

timestamps['next_point'] = time.time()

print('Summary of times with Radial Search method:\n')
print('{:<10s} {:>10s}'.format('Process', 'Time (ms)'))
print('-' * 25)

last = timestamps['start']

for key in timestamps:
    if key == 'start':
        continue
    time_ms = 1000*(timestamps[key] - last)
    print('{:<10s} {:>10.3f}'.format(key, time_ms))
    last = timestamps[key]

print('-' * 25)

total_time_ms = 1000*(last - timestamps['start'])
print('{:<10s} {:>10.3f}'.format('Total', total_time_ms))

print('=' * 25)