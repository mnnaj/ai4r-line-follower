import cv2
import os
import random
import pandas as pd
import streamlit as st
from process import ImageProcessor, BNSearch

path = 'samples/streamcam/'

cfg = {}

st.title("Line Detection Test")

# ----------------------- Image Selection ----------------------- #

st.subheader("Select a sample image")

tape_type = st.radio(
    "Select tape type",
    ("old tape (light green)", "new tape (vibrant green)"))

if tape_type == "old tape (light green)":
    path += 'old_tape/'
else:
    path += 'new_tape/'

selection_type = st.radio(
    "Selection:", ("random", "user selection"))

if selection_type == "random":
    file_list = os.listdir(path)
    selected_file = random.choice(file_list)
else:
    file_list = os.listdir(path)
    selected_file = st.selectbox("Select a file", file_list)

# Add a button to randomly select another image
if st.button("Different Image"):
    selected_file = random.choice(file_list)
    selected_file_path = os.path.join(path, selected_file)
    # Use the selected_file_path for further processing

st.write("Selected file: ", selected_file)

selected_image_path = os.path.join(path, selected_file)

# ----------------------- Image Viz ----------------------- #

image = cv2.imread(selected_image_path)
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

show = st.checkbox("Show Original Image")
if show:
    st.image(image, caption="Original Image", use_column_width=True)

st.markdown("""---""")

processor = ImageProcessor(image)

hls = st.checkbox("Show HLS")
h, l, s = processor.get_hls()

if hls:
    st.image(h, caption="Hue", use_column_width=True)
    st.image(l, caption="Lightness", use_column_width=True)
    st.image(s, caption="Saturation", use_column_width=True)

# ----------------------- Thresholding ----------------------- #

st.write("Channels to select for thresholding:")

hls_select = st.multiselect("Select HLS Channels", ["Hue", "Lightness", "Saturation"], default=["Hue", "Saturation"])
cfg['threshold_channels'] = hls_select

if hls_select:
    thresholded_images = []
    st.subheader("Selected Channels")
    for channel in hls_select:
        if channel == "Hue":
            st.subheader("Hue Channel (Thresholded)")
            #st.image(h, use_column_width=True)
            range = st.slider("Select the range for thresholding", 0, 255, (20, 50))
            cfg['h_thresh'] = tuple(range)
            h_binary = processor.threshold(h, thresh = tuple(range))
            st.image(h_binary, use_column_width=True)
            thresholded_images.append(h_binary)
            #st.subheader("Thresholded Hue Channel")
        elif channel == "Lightness":
            st.subheader("Lightness Channel (Thresholded)")
            #st.image(l, use_column_width=True)
            range = st.slider("Select the range for thresholding", 0, 255, (150, 255))
            cfg['l_thresh'] = tuple(range)
            st.subheader("Thresholded Lightness Channel")
            l_binary = processor.threshold(l, thresh = tuple(range))
            st.image(l_binary, use_column_width=True)
            thresholded_images.append(l_binary)
        elif channel == "Saturation":
            st.subheader("Saturation Channel (Thresholded)")
            #st.image(s, use_column_width=True)
            range = st.slider("Select the range for thresholding", 0, 255, (100, 255))
            cfg['s_thresh'] = tuple(range)
            s_binary = processor.threshold(s, thresh = tuple(range))
            st.image(s_binary, use_column_width=True)
            thresholded_images.append(s_binary)

if len(hls_select) == 0:
    st.write("Please select at least one channel to proceed")
else:
    binary = processor.combine(thresholded_images)
    st.subheader("Final Thresholded Image")
    if len(hls_select) == 1:
        st.write("This image is the result of thresholding the channel: ", hls_select[0])
    else:
        ch_str = ""
        for channel in hls_select:
            ch_str += channel + ", "
        st.write("This image is the result of bitwise AND operation the channels: {}".format(ch_str))
    st.image(binary, use_column_width=True)

st.markdown("""---""")

# ----------------------- Morphological Operations ----------------------- #

st.subheader("Morphological operations")
st.write("Would you like to apply any morphological operations?")


erode = st.checkbox("Erode", False)
cfg['erode'] = erode

if erode and len(hls_select) > 0:
    st.subheader("Eroded Image")
    kernel_erode = st.slider("Select the kernel size for erosion operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_erode'] = kernel_erode
    binary = processor.erode(binary, kernel_erode)
    st.image(binary, use_column_width=True)

dilate = st.checkbox("Dilate", False)
cfg['dilate'] = dilate

if dilate and len(hls_select) > 0:
    st.subheader("Dilated Image")
    kernel_dilate = st.slider("Select the kernel size for dilation operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_dilate'] = kernel_dilate
    binary = processor.dilate(binary, kernel_dilate)
    st.image(binary, use_column_width=True)

opening = st.checkbox("Open", False)
cfg['opening'] = opening
if opening and len(hls_select) > 0:
    st.subheader("Opened Image")
    kernel_open = st.slider("Select the kernel size for opening operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_open'] = kernel_open
    binary = processor.open(binary, kernel_open)
    st.image(binary, use_column_width=True)

close = st.checkbox("Close", False)
cfg['close'] = close
if close and len(hls_select) > 0:
    st.subheader("Closed Image")
    kernel_close = st.slider("Select the kernel size for closing operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_close'] = kernel_close
    binary = processor.close(binary, kernel_close)
    st.image(binary, use_column_width=True)

# ----------------------- Contour Detection ----------------------- #

st.markdown("""---""")

largcon = st.checkbox("Get largest contour", True)
cfg['largcon'] = largcon
if largcon and len(hls_select) > 0:
    st.subheader("Largest Contour")
    contours, binary = processor.get_largest_contour(binary)
    st.image(binary, use_column_width=True)
    viz = st.checkbox("Visualize Contours")
    if viz:
        st.subheader("All Contours")
        img_contours = processor.draw_contours(contours)
        st.image(img_contours, use_column_width=True)

st.markdown("""---""")

# ----------------------- Line Detection ----------------------- #

st.subheader("Line Detection")

st.write("Which shape would you like to use for the search?")
shape = st.radio("Select a shape", ("Circle", "Ellipse"), index = 1)
cfg['shape'] = shape

draw = st.checkbox("Show search results", value = True)
draw_radius = 3

if shape == "Circle":
    st.subheader("Parameters for Circle Search")
    radius = st.slider("Select the radius of the search", min_value = 10, max_value = 100, step = 10, value = 50)
    cfg['radius'] = radius
    start_angle, end_angle = st.slider(
        "Select the arc angles to search within",
        min_value = 90,
        max_value = 450,
        value = (210, 330),
        step = 10
    )
    cfg['angles'] = (start_angle, end_angle)
    searcher = BNSearch(
        search_method = 'circle',
        radius = radius,
        start_angle = start_angle,
        end_angle = end_angle,
        draw = draw,
        draw_radius = draw_radius
    )
if shape == "Ellipse":
    st.subheader("Parameters for Ellipse Search")
    a = st.slider("Select the major axis length", min_value = 20, max_value = 200, step = 10, value = 100)
    b = st.slider("Select the minor axis length", min_value = 10, max_value = 100, step = 10, value = 50)
    cfg['ellipse_ab'] = (a, b)
    start_angle, end_angle = st.slider(
        "Select the arc angles to search within",
        min_value = 90,
        max_value = 450,
        value = (210, 330),
        step = 10
    )
    cfg['angles'] = (start_angle, end_angle)
    searcher = BNSearch(
        search_method = 'ellipse',
        a = a,
        b = b,
        start_angle = start_angle,
        end_angle = end_angle,
        draw = draw,
        draw_radius = draw_radius
    )

st.subheader("Search Results")
points = searcher(binary)
df = pd.DataFrame(points, columns=["X", "Y"])

st.image(searcher.img_color, use_column_width=True)

st.write("Number of points found: ", len(points))
st.write("Co-ordinates of the points:")
st.write(df)