
from process import ImageProcessor, BNSearch
import cv2
import numpy as np
import matplotlib.pyplot as plt

image = cv2.imread('samples/gray_carpet_sharp_right.jpg')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

processor = ImageProcessor(image)

show_original = True
undistort = True
warp = True
thresh = ['h', 's']
show_hls = False
h_thresh = (20, 50)
l_thresh = (150, 255)
s_thresh = (100, 255)
show_thresholded = True
largcon = True
blur = True
kernel = 13
thinning = False
bns = True

if show_original:
    plt.imshow(image)
    plt.title("Original Image")
    plt.axis('off')
    #plt.show()

if undistort:
    undistorted = processor.undistort(image)
    plt.imshow(undistorted)
    plt.title("Undistorted Image")
    plt.axis('off')
    #plt.show()

if warp:
    p1 = (0.40, 0.40)
    p2 = (0.85,0.40)
    p3 = (0, 1)
    p4 = (1, 1)

    points = np.float32([p1, p2, p3, p4])

    image_points = processor.draw_points(points)
    # plt.imshow(image_points)
    # plt.title("ROI for Perspective Warp")
    # plt.axis('off')
    #plt.show()

    warped = processor.warp(points)        
    plt.imshow(warped)
    plt.title("Perspective transformed image")
    plt.axis('off')
    #plt.show()

h, l, s = processor.get_hls()

if show_hls:
    fig, axs = plt.subplots(3, 1, figsize=(8, 6))
    axs[0].imshow(h, cmap='gray')
    axs[0].set_title("Hue Channel")
    axs[0].axis('off')

    axs[1].imshow(l, cmap='gray')
    axs[1].set_title("Lightness Channel")
    axs[1].axis('off')

    axs[2].imshow(s, cmap='gray')
    axs[2].set_title("Saturation Channel")
    axs[2].axis('off')

    plt.show()

thresholded_images = []

if 'h' in thresh:
    h_binary = processor.threshold(h, h_thresh)
    thresholded_images.append(h_binary)
    plt.imshow(h_binary, cmap='gray')
    plt.title("Hue Channel Thresholded")
    plt.axis('off')
    #plt.show()

if 'l' in thresh:
    l_binary = processor.threshold(l, l_thresh)
    thresholded_images.append(l_binary)
    plt.imshow(l_binary, cmap='gray')
    plt.title("Lightness Channel Thresholded")
    plt.axis('off')
    #plt.show()

if 's' in thresh:
    s_binary = processor.threshold(s, s_thresh)
    thresholded_images.append(s_binary)
    plt.imshow(s_binary, cmap='gray')
    plt.title("Saturation Channel Thresholded")
    plt.axis('off')
    #plt.show()

if len(thresholded_images) > 0:
    binary = processor.combine(thresholded_images)
    plt.imshow(binary, cmap='gray')
    plt.title("Combined Thresholded Image")
    plt.axis('off')
    #plt.show()

    if largcon:
        contours, binary = processor.get_largest_contour(binary)
        plt.imshow(binary, cmap='gray')
        plt.title("Largest Contour")
        plt.axis('off')
        #plt.show()

    if blur:
        binary = processor.blur(binary, kernel)
        plt.imshow(binary, cmap='gray')
        plt.title("Blurred Image")
        plt.axis('off')
        #plt.show()
    
    if thinning:
        binary = processor.thinning(binary)
        plt.imshow(binary, cmap='gray')
        plt.title("Thinned Image")
        #plt.axis('off')
        #plt.show()
        points = processor.get_thinned_points(binary)
        #breakpoint()
        X = [x for x, _ in points]
        Y = [y for _, y in points]
        plt.scatter(X, Y, c='r', s=3)
        #plt.title("Thinned Points")
        plt.axis('off')
        plt.show()
    
    if bns:
        searcher = BNSearch(
            search_method = 'line',
            radius = 100,
            start_angle = 210,
            end_angle = 330,
        )
        points = searcher(binary)
        image_points = searcher.img_color
        plt.imshow(image_points)
        plt.title("BN Search")
        plt.axis('off')
        plt.show()
        print(points)