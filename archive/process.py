import pickle
import cv2
import numpy as np

# PRELOAD THE CALIBRATION FILES
# cal_path='camera_cal/cal_pickle.p'
# with open(cal_path, mode='rb') as f:
#     file = pickle.load(f)
# mtx = file['mtx']
# dist = file['dist']

class ImageProcessor:
    def __init__(self, image):
        self.image = image
        self.original = image.copy()
    
    def undistort(self, img, cal_path='camera_cal/cal_pickle.p'):
        """
        Undistort image using camera calibration parameters
        """
        with open(cal_path, mode='rb') as f:
            file = pickle.load(f)
        mtx = file['mtx']
        dist = file['dist']
        dst = cv2.undistort(img, mtx, dist, None, mtx)
        self.image = dst
        return dst
    
    def draw_points(self, src):
        image_points = self.image.copy()
        img_size = np.float32([(self.image.shape[1],self.image.shape[0])])
        src = src * img_size

        pts = np.int32(src).reshape((-1,1,2))
        pts = pts[[0,1,3,2],:,:]

        image_points = cv2.polylines(image_points,[pts],True,(0,255,255),2)
        return image_points

    def warp(self,
            src,
            dst_size=(1280,720),
            dst=np.float32([(0,0), (1, 0), (0,1), (1,1)])):
        img_size = np.float32([(self.image.shape[1],self.image.shape[0])])
        src = src * img_size
        dst = dst * np.float32(dst_size)
        # Given src and dst points, calculate the perspective transform matrix
        M = cv2.getPerspectiveTransform(src, dst)
        # Warp the image using OpenCV warpPerspective()
        warped = cv2.warpPerspective(self.image, M, dst_size)
        self.image = warped
        return warped
    
    def color_balance(self):
        """
        Apply Color Balance Adjustment to the image
        (Expected input is in RGB format)
        """
        result = cv2.cvtColor(self.image, cv2.COLOR_RGB2LAB)
        avg_a = np.average(result[:, :, 1])
        avg_b = np.average(result[:, :, 2])
        result[:, :, 1] = result[:, :, 1] - ((avg_a - 128) * (result[:, :, 0] / 255.0) * 1.1)
        result[:, :, 2] = result[:, :, 2] - ((avg_b - 128) * (result[:, :, 0] / 255.0) * 1.1)
        result = cv2.cvtColor(result, cv2.COLOR_LAB2RGB)
        self.image = result
        return result

    def get_hls(self):
        """
        Convert the image from RGB to HLS color space
        And return the H, L and S channels
        Input is expected to be in RGB format
        """
        hls = cv2.cvtColor(self.image, cv2.COLOR_RGB2HLS)
        h_channel = hls[:, :, 0]
        l_channel = hls[:, :, 1]
        s_channel = hls[:, :, 2]
        return h_channel, l_channel, s_channel
    
    @staticmethod
    def threshold(img, thresh = (150, 255)):
        """
        Apply a binary threshold to the image
        """
        binary_output = np.zeros_like(img)
        binary_output[(img >= thresh[0]) & (img <= thresh[1])] = 255
        return binary_output
    
    @staticmethod
    def blur(img, kernel_size=3):
        """
        Apply a Gaussian Blur to the image
        """
        return cv2.GaussianBlur(img, (kernel_size, kernel_size), 0)
    
    @staticmethod
    def thinning(img):
        """
        Apply Thinning to the image
        """
        return cv2.ximgproc.thinning(img)
    
    @staticmethod
    def get_largest_contour(img):
        """
        Get the largest contour from the image
        """
        contours, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)

        img = np.zeros_like(img)
        cv2.drawContours(img, [contours[0]], -1, 255, -1)
        return contours, img
    
    def draw_contours(self, contours):
        """
        Visualize the contours on the image
        """
        #img = np.zeros_like(self.image)
        img = self.image.copy()
        cv2.drawContours(img, contours, -1, (0, 255, 0), 3)
        return img
    
    @staticmethod
    def combine(images):
        """
        Combine multiple binary images through a bitwise AND operation
        """
        if len(images) == 1:
            return images[0]
        else:
            return cv2.bitwise_and(*images)

    @staticmethod
    def get_points(image, step = 50):
        # Get points on the image
        points = np.argwhere(image > 0)
        # Sort the points by x
        points = points[points[:,0].argsort()]
        y = points[-1][0]
        x_values = []
        y_values = []
        x_step = None
        while y > 0:
            x = points[points[:,0] == y][:,1]
            x = x[len(x)//2]    # median
            x_values.append(x)
            y_values.append(y)
            y -= step
            # Calculate angle between most recent points
            if len(y_values) > 2:
                angle = np.arctan2(x_values[-1] - x_values[-2], y_values[-1] - y_values[-2])
                angle = np.degrees(angle)
                if angle > 0 and angle < 135:   # The line is moving right
                    x_step = step
                    break
                elif angle < 0 and angle > -135:    # The line is moving left
                    x_step = -step
                    break
        
        if x_step:
            while x > 0 and x < image.shape[1]:
                try:
                    y = points[points[:,1] == x][:,0]
                    y = y[len(y)//2]    # median
                    x_values.append(x)
                    y_values.append(y)
                    x += x_step
                except:
                    break
        
        points = list(zip(x_values, y_values))
        return points

    @staticmethod
    def plot_points(image, points):
        # convert img to color
        img = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        # create an image with the same shape as img
        #img = np.zeros_like(img)
        for point in points:
            cv2.circle(img, point, 5, (255, 0, 0), -1)
        return img

    @staticmethod
    def open(image, kernel_size=5, iterations=1):
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
        opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel, iterations=iterations)
        return opening

    @staticmethod
    def close(image, kernel_size=5, iterations=1):
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
        closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel, iterations=iterations)
        return closing

    @staticmethod
    def erode(image, kernel_size=5, iterations=1):
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
        erosion = cv2.erode(image, kernel, iterations=iterations)
        return erosion

    @staticmethod
    def dilate(image, kernel_size=5, iterations=1):
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
        dilation = cv2.dilate(image, kernel, iterations=iterations)
        return dilation

class BNSearch:
    """
    Search for the lane using the BN Method
    """
    def __init__(
            self,
            search_method ='circle',
            radius = 100,
            start_angle = 180,
            end_angle = 360,
            patience=0,
            draw = True,
            draw_radius = 5,
            a = 100,
            b = 50
            ):
        self.img = None
        self.img_color = None
        self.radius = radius
        self.start_angle = start_angle
        self.end_angle = end_angle
        self.search = search_method
        self.patience = patience
        self.counter = 0
        self.draw = draw
        self.draw_radius = draw_radius
        self.a = a
        self.b = b

    def get_starting_point(self):
        """
        Get the starting point of the lane
        The starting point is ideally the bottom center of the lane
        If the lane is not found at bottom center
        The function will move up the image until it finds the lane
        If the lane is not found in the bottom half, the function will return None
        """
        last_idx = -1
        while abs(last_idx) < self.img.shape[0]//2:
            last_row = self.img[last_idx, :]
            lane_exists = np.where(last_row > 0)[0]
            if len(lane_exists) > 0:   # if lane exists
                lane_center = np.median(lane_exists)
                center_point = (int(lane_center), (self.img.shape[0] + last_idx))
                return center_point
            # Else move up by 1 pixel
            last_idx -= 1
        return None

    def get_points_on_line(self, start, angle):
        """
        Get points on a line given start point, angle, and length
        """
        points = []
        for dist in range():
            x = int(start[0] + self.radius * np.cos(np.deg2rad(angle)))
            y = int(start[1] + self.radius * np.sin(np.deg2rad(angle)))
            points.append((x, y))
        return points

    def get_points_on_circle(self, center):
        """
        Get points on a circle
        """
        points = []
        for angle in np.arange(self.start_angle, self.end_angle, 1):
            x = int(center[0] + self.radius * np.cos(np.deg2rad(angle)))
            y = int(center[1] + self.radius * np.sin(np.deg2rad(angle)))
            points.append((int(x), int(y)))
        return points
    
    def get_points_on_ellipse(self, center, angle=0):
        """
        Get points on an ellipse with rotated axes
        """
        points = []
        alpha = np.deg2rad(angle)
        for theta in np.arange(self.start_angle, self.end_angle, 1):
            x = int(center[0] + self.a * np.cos(np.deg2rad(theta)) * np.cos(alpha) - self.b * np.sin(np.deg2rad(theta)) * np.sin(alpha))
            y = int(center[1] + self.a * np.cos(np.deg2rad(theta)) * np.sin(alpha) + self.b * np.sin(np.deg2rad(theta)) * np.cos(alpha))
            points.append((int(x), int(y)))
        return points
    
    def get_next_point(self, current_point, delta = 0):
        """
        Get the next point on the lane
        Returns None if no point is found
        """
        if self.draw:
            cv2.circle(self.img_color, current_point, self.draw_radius, (255, 0, 0), -1)

        if self.search == 'line':
            points = self.get_points_on_line(current_point, angle = delta, length = self.radius)
        elif self.search == 'circle':
            points = self.get_points_on_circle(current_point)
        elif self.search == 'ellipse':
            points = self.get_points_on_ellipse(current_point, angle = delta)
        
        # Check if any point is out of bounds
        for point in points:
            if point[0] < 0 or point[0] > self.img.shape[1] or point[1] < 0 or point[1] > self.img.shape[0]:
                return None
        
        points_on_lane = []
        #breakpoint()
        for point in points:
            if self.img[point[1], point[0]] > 0:
                points_on_lane.append(point)

        if len(points_on_lane) == 0:
            if self.patience > 0:
                points_on_lane = points
                self.counter += 1
                if self.counter > self.patience:
                    return None
                # TODO: Need to raise a flag to indicate that the lane is not found
            else:
                return None
        
        median_idx = int(len(points_on_lane) / 2)
        next_point = points_on_lane[median_idx]

        if self.draw:
            #cv2.circle(self.img_color, current_point, 5, (255, 0, 0), -1)
            # Draw the search points on the image
            for point in points:
                cv2.circle(self.img_color, point, self.draw_radius, (0, 255, 0), -1)
            # Draw the next point on the image
            cv2.circle(self.img, next_point, self.draw_radius, (0, 0, 255), -1)        

        return next_point
    
    def __call__(self, image):
        self.img = image
        start = self.get_starting_point()
        #print('Starting point: ' , start)
        if start is None:
            return None
        
        points = [start,]

        self.img_color = self.img.copy()
        self.img_color = cv2.cvtColor(self.img_color, cv2.COLOR_GRAY2RGB)

        next_point = self.get_next_point(start)
        points.append(next_point)

        while next_point is not None:
            # Calculate the angle between the last two points
            theta = np.arctan2(points[-1][1] - points[-2][1], points[-1][0] - points[-2][0])
            delta = int(90 + np.rad2deg(theta))
            # Get next point and append to points list
            next_point = self.get_next_point(next_point, delta = delta)
            if next_point is not None:
                points.append(next_point)
        
        return points