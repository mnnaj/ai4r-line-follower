import os
import streamlit as st

def list_files_in_directory(directory):
    files = os.listdir(directory)
    return files

def main():
    st.title("File Selector")

    # Create an input box for directory path
    directory_path = st.text_input("Enter the directory path:", "samples/")

    if directory_path:
        if os.path.exists(directory_path):
            st.write(f"Selected Directory: {directory_path}")

            # Get the list of files in the selected directory
            files = list_files_in_directory(directory_path)

            # Display a drop-down box with all files in the directory
            selected_file = st.selectbox("Select a file:", files)

            if selected_file:
                st.write(f"Selected File: {selected_file}")
        else:
            st.write("Invalid directory path!")

if __name__ == "__main__":
    main()
