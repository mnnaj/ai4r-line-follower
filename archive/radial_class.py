import cv2
import numpy as np
import time

timestamps = {}

timestamps['start'] = time.time()

radius = 50

def create_circle_hash_table(radius):
    hash_table = {
        'x': np.zeros(361, dtype=np.int8),
        'y': np.zeros(361, dtype=np.int8)
    }
    for angle in range(361):
        hash_table['x'][angle] = int(radius * np.cos(np.deg2rad(angle)))
        hash_table['y'][angle] = int(radius * np.sin(np.deg2rad(angle)))
    return hash_table

hash_table = create_circle_hash_table(radius)

class LineDetector:
    def __init__(
            self, img, start_angle=210, end_angle=330):
        self.img = img
        self.img_shape_0 = img.shape[0]
        self.img_shape_1 = img.shape[1]
        self.start_angle = start_angle
        self.end_angle = end_angle
    
    def __call__(self):
        start = self.get_starting_point()
        if start is None: return None

        points = [start, ]
        next_point = self.get_next_point(start)

        while next_point is not None:
            theta = np.arctan2(points[-1][1] - points[-2][1], points[-1][0] - points[-2][0])
            delta = int(90 + np.rad2deg(theta))
            next_point = self.get_next_point(next_point, delta)
            if next_point is not None: points.append(next_point)
 
        return points

    def get_next_point(self, current_point, delta = 0):
        points = self.get_points_on_circle(current_point, delta)

        # Check if any point is out of bounds
        valid_points_mask = np.logical_and.reduce((
            points[:, 0] >= 0,
            points[:, 0] < self.img_shape_1,
            points[:, 1] >= 0,
            points[:, 1] < self.img_shape_0
        ))

        # Return None if any of the points is invalid
        if not np.all(valid_points_mask): return None
        
        # TODO: Instead of stopping, we can just filter out valid points, 
        # and only stop if there are no more valid points to search for
        
        valid_mask = self.img[tuple(points.T)] > 0
        points_on_lane = points[valid_mask]

        # Return none if no point is found
        if len(points_on_lane) == 0: return None
        
        # Return the median point (next point)
        return points_on_lane[int(len(points_on_lane)/2)]

    def get_points_on_circle(self, center, delta = 0):
        """
        Get points on a circle using the hash table
        """
        angles = np.arange(self.start_angle + delta, self.end_angle + delta)
        x_offsets = hash_table['x'][angles]
        y_offsets = hash_table['y'][angles]
        points = np.column_stack((x_offsets + center[0], y_offsets + center[1]))
        return points    
    
    def get_starting_point(self):
        last_idx = -1
        while abs(last_idx) < self.img_shape_0//2:
            last_row = self.img[last_idx, :]
            lane_exists = np.where(last_row > 0)[0] # lane exists where there is a nonzero value
            if len(lane_exists) > 0: # If lane exists
                lane_center = np.median(lane_exists)
                center_point = (int(lane_center), (self.img_shape_0 + last_idx))
                return center_point
            # Else move up by 1 pixel
            last_idx -+ 1
        return None

#timestamps = {}

timestamps['init'] = time.time()

image_path = 'samples/gray_carpet_straight.jpg'
image = cv2.imread(image_path)

timestamps['load'] = time.time()

hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
_, _, s_channel = cv2.split(hls)

timestamps['hls'] = time.time()

img = cv2.threshold(s_channel, 100, 255, cv2.THRESH_BINARY)[1]

timestamps['threshold'] = time.time()

points = LineDetector(img)()

timestamps['detection'] = time.time()

print('Summary of times with Radial Search method:\n')
print('{:<10s} {:>10s}'.format('Process', 'Time (ms)'))
print('-' * 25)

last = timestamps['start']

for key in timestamps:
    if key == 'start':
        continue
    time_ms = 1000*(timestamps[key] - last)
    print('{:<10s} {:>10.3f}'.format(key, time_ms))
    last = timestamps[key]

print('-' * 25)

total_time_ms = 1000*(last - timestamps['start'])
print('{:<10s} {:>10.3f}'.format('Total', total_time_ms))

print('=' * 25)