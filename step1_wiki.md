# Frame Capture Tool - User Guide

The Frame Capture Tool is a Streamlit web application designed to capture frames from your camera feed and save them to your computer. This guide will walk you through the objectives and step-by-step instructions on how to use this tool effectively.

## Objectives

The Frame Capture Tool has the following objectives:

1. **Capture Frames:** The tool allows you to capture a specified number of frames from your camera feed at a chosen resolution and frame rate.

2. **Adjust Settings:** You can customize various settings, including resolution, frame rate, and exposure, to control the quality and appearance of the captured frames.

3. **Save Captured Frames:** The captured frames are automatically saved in a designated directory on your computer.

## Instructions

Follow these instructions to use the Frame Capture Tool:

1. **Access the Application:**
   - Ensure you have Python installed on your computer.
   - Install the required libraries by running the command: `pip install cv2 streamlit`
   - Download the provided code and save it as a `.py` file.
   - Open your terminal or command prompt and navigate to the directory containing the `.py` file.
   - Run the application using the command: `streamlit run your_filename.py`.

2. **Application Interface:**
   - Once the application starts, you'll see the main interface divided into two columns.

3. **Instructions:**
   - On the left column, there is a checkbox labeled "Show Instructions." Toggle this checkbox to show or hide the application instructions.

4. **Run Name:**
   - In the same left column, you'll find a text input labeled "Enter name of run." This is where you can provide a unique name for the run.
   - The application suggests a default name based on the previous runs.

5. **Number of Frames:**
   - Specify the number of frames you want to capture in the "Enter number of frames to capture" input box.
   - Use the provided up and down arrows or directly type the desired number.

6. **Resolution and Frame Rate:**
   - Select your desired resolution from the "Select resolution" radio button options (e.g., 480p, 540p, 720p, 1080p).
   - Depending on the selected resolution, available frame rates will be displayed in the "Select frame rate" radio button options. Choose a suitable frame rate.

7. **Exposure Adjustment:**
   - Use the "Exposure" slider to adjust the camera's exposure setting. Drag the slider left or right to increase or decrease the exposure.

8. **Start Capture:**
   - Click the "Start Capture" button to begin capturing frames.
   - The application will start capturing the specified number of frames based on your settings.
   - A live camera feed will be displayed in the right column, showing the current frame being captured.

9. **Frame Capture Progress:**
   - As frames are captured, you will see a text notification indicating the progress (e.g., "Capturing frame 1 of 100...").
   - The captured frames will be temporarily stored in the application's memory.

10. **Saving Captured Frames:**
    - Once the desired number of frames is captured, the application will automatically save them to your computer.
    - You will see a notification indicating that the frames are being saved.
    - After saving, a "Done!" message will appear, confirming the successful completion of the capture process.

11. **Stop Stream:**
    - If you want to stop the frame capturing process before reaching the specified number of frames, click the "Stop Stream" button.
    - The application will complete the saving process for the captured frames and then stop.

12. **Stream Information:**
    - Throughout the capturing process, the right column displays essential stream information, including resolution, frame rate, and exposure.

13. **Exit the Application:**
    - After capturing and saving frames, you can exit the application by closing the terminal or command prompt window.

By following these instructions, you can easily use the Frame Capture Tool to capture and save frames from your camera feed for various purposes, such as research, analysis, and creative projects.