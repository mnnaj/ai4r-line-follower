import cv2
import os
import numpy as np
from detector_lib import ImageProcessor, load_config
import random

def find_nearest_point(test_point, benchmark_points):
    min_distance = float('inf')
    nearest_point = None
    for point in benchmark_points:
        distance = np.linalg.norm(np.array(test_point) - np.array(point))
        if distance < min_distance:
            min_distance = distance
            nearest_point = point
    return nearest_point

def calculate_mse(test_points, benchmark_points):
    total_mse = 0
    for test_point in test_points:
        nearest_point = find_nearest_point(test_point, benchmark_points)
        mse = np.linalg.norm(np.array(test_point) - np.array(nearest_point)) ** 2
        total_mse += mse
    return total_mse / len(test_points)

img_dir = 'samples/streamcam/new_tape/'

imgs = os.listdir(img_dir)
img_name = random.choice(imgs)

img_path = os.path.join(img_dir, img_name)

#img_path = 'samples/streamcam/new_tape/test_2023-05-31_12-07-25.jpg'

config_bench = load_config('configs/config_benchmark.yaml')
config_test = load_config('configs/config_1.yaml')

ip_benchmark = ImageProcessor(config_bench)
ip_test = ImageProcessor(config_test)

# Load Image
img = cv2.imread(img_path)

points_bench, _ = ip_benchmark.process(img)
points_test, _ = ip_test.process(img)

# Convert image from BGR to RGB
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# Draw points on the image
for point in points_bench:
    cv2.circle(img, point, 2, (0, 255, 0), -1)  # Green for benchmark points

for point in points_test:
    cv2.circle(img, point, 2, (0, 0, 255), -1)  # Red for test points


# Show the image with points
cv2.imshow('Line Detection Comparison', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

mse = calculate_mse(points_test, points_bench)
print("Mean Squared Error (MSE): of distance from nearest point: ", mse)


def distance_to_line(point, line_point1, line_point2):
    # Calculate the perpendicular distance from 'point' to the line formed by 'line_point1' and 'line_point2'
    x0, y0 = point
    x1, y1 = line_point1
    x2, y2 = line_point2

    return np.abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1) / np.sqrt((y2 - y1)**2 + (x2 - x1)**2)

def calculate_mse_for_point(test_point, benchmark_points):
    # Find the two nearest points on the benchmark line to the test point
    nearest_points = sorted(benchmark_points, key=lambda p: np.linalg.norm(np.array(test_point) - np.array(p)))
    nearest_point1, nearest_point2 = nearest_points[:2]
    
    # Calculate MSE for the test point
    mse = distance_to_line(test_point, nearest_point1, nearest_point2)**2
    return mse

# Calculate MSE for each point on the test line
total_mse = 0
for test_point in points_test:
    mse = calculate_mse_for_point(test_point, points_bench)
    total_mse += mse

mean_squared_error = total_mse / len(points_test)
print("Mean Squared Error (MSE) of distance from line:", mean_squared_error)
